//
//  DataManager.h
//  HangmanWithEditor
//
//  Created by User on 11/23/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>
#import "CategoryEntry.h"
#import "QuestionEntry.h"
@interface DataManager : NSObject
@property (nonatomic, strong)  NSManagedObjectContext *context;

+ (id)sharedManager;
-(void) createQuestion: (NSString*) question withAnswer: (NSString*) answer andCategory: (NSString*) category;
-(void) createNewCategory:(NSString*) name;
-(NSMutableArray*) getCategories;
-(NSMutableArray*) getQuestions;
-(void) deleteAllCategories;
-(void) deleteAllQuestions;
-(NSMutableArray*) getQuestionsInCategory: (NSString*) name;
@end
