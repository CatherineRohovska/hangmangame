//
//  QuestionEntry.h
//  HangmanWithEditor
//
//  Created by User on 11/23/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface QuestionEntry : NSManagedObject
@property (nonatomic, strong) NSString* answer;
@property (nonatomic, strong) NSString* question;
@end
