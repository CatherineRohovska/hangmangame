//
//  DrawableView.h
//  HangmanWithEditor
//
//  Created by User on 11/25/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DrawableView : UIView
-(void)animateStrokeEnd: (NSInteger) stage;
- (void) animationCompleteImmediately;
-(void) resetSublayers;
@end
