//
//  GameViewController.m
//  HangmanWithEditor
//
//  Created by User on 11/24/15.
//  Copyright © 2015 User. All rights reserved.
//
#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)
#import "GameViewController.h"
#import "DataManager.h"
#import "AlphabeticCollectionViewCell.h"
#import "DrawableView.h"
@interface GameViewController ()
{
    DataManager* manager;
    NSMutableArray* questionsInCategory;
    QuestionEntry* currentQuestion;
    NSInteger countOfAttempts;
    DrawableView *hangmanDrawingView;
    NSMutableArray* arrayOfCrossMarks;
    NSMutableArray* arrayOfButtons;
    NSInteger buttonSide;
    NSInteger spaceBetweenButtons;
    bool firstLoad;
    //
    NSInteger hintsCount;

}

@end
static const NSString* alphabet = @"ABCDEFGHIJKLMNOPQRSTUVWXYZ";
@implementation GameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    manager = [DataManager sharedManager];
    questionsInCategory = [manager getQuestionsInCategory:_category];
    [self getRandomQuestion];
     arrayOfCrossMarks = [[NSMutableArray alloc] init];
    arrayOfButtons = [[NSMutableArray alloc] init];
  
      countOfAttempts = 0;
    firstLoad = NO;
    _questionTextLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _questionTextLabel.numberOfLines = 3;

    hintsCount = 3;
    //
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:[@"Hints:" stringByAppendingString:[@(hintsCount) stringValue]] style:UIBarButtonItemStylePlain target:self action:@selector(actionOnHint:)];
    self.navigationItem.rightBarButtonItem = anotherButton;
}
-(void) actionOnHint: (UIBarButtonItem*) sender
{
    if (hintsCount!=0) {
         hintsCount--;
        NSInteger answerLettersCount = [ _answerLetterCollection.visibleCells count];
      
        NSInteger randomIndex = arc4random_uniform((u_int32_t )answerLettersCount);
          AlphabeticCollectionViewCell* cell = [_answerLetterCollection.visibleCells objectAtIndex:randomIndex];
        while (cell.openState) {
            randomIndex = arc4random_uniform((u_int32_t )answerLettersCount);
            cell = [_answerLetterCollection.visibleCells objectAtIndex:randomIndex];
        }
       
     NSInteger buttonIndex = [alphabet rangeOfString:cell.textLabel.text].location;
     UIButton* button = [arrayOfButtons objectAtIndex:buttonIndex];
     [self checkLetter:button];
    }
    [sender setTitle:[@"Hints:" stringByAppendingString:[@(hintsCount) stringValue]]];
    
    
}
-(void) recoverHintsAfterChangeQuestion
{
    hintsCount = 3;
    [self.navigationItem.rightBarButtonItem setTitle:[@"Hints:" stringByAppendingString:[@(hintsCount) stringValue]]];
}
- (void) createButtons
{
    //
    spaceBetweenButtons = 10;
    buttonSide = (_answerLetterCollection.bounds.size.width-hangmanDrawingView.frame.size.width - 9*spaceBetweenButtons)/9;
    //
    CGRect frame = CGRectMake(_answerLetterCollection.frame.origin.x, _answerLetterCollection.frame.origin.y+_answerLetterCollection.frame.size.height-buttonSide, buttonSide, buttonSide);
    for (int i=0; i<26; i++) {
        UIButton* charButton = [UIButton buttonWithType: UIButtonTypeCustom];
        charButton.showsTouchWhenHighlighted = YES;
        if (i%9==0) { frame.origin.y = frame.origin.y+spaceBetweenButtons+buttonSide; frame.origin.x =20;}
        else {frame.origin.x = frame.origin.x+spaceBetweenButtons+buttonSide;}
        [charButton setFrame:frame];
        NSRange symbolRange = NSMakeRange(i, 1);
        [charButton setTitle:[alphabet substringWithRange:symbolRange] forState:UIControlStateNormal];
        charButton.layer.borderWidth = 2.0f;
        charButton.layer.borderColor = [[UIColor whiteColor] CGColor];
        [charButton addTarget:self
                       action:@selector(checkLetter:)forControlEvents:UIControlEventTouchUpInside];
        [arrayOfButtons addObject:charButton];
        [self.view addSubview:charButton];
        
        
        
    }
    
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
   // frame = _hangmanPlaceholderView.frame;
    if (countOfAttempts==0) {
        [hangmanDrawingView removeFromSuperview];
        hangmanDrawingView = [[DrawableView alloc] initWithFrame:_hangmanPlaceholderView.frame];
       
        [self.view addSubview:hangmanDrawingView];
        [hangmanDrawingView animateStrokeEnd:countOfAttempts];
        
    }
    if(!firstLoad)
    {
        [self createButtons];
        firstLoad = YES;
    }
  
    
    
}
-(void) checkLetter:(UIButton*)sender
{
    [hangmanDrawingView animationCompleteImmediately];
    NSLog(@"%@", sender.titleLabel.text);
    bool result = [self checkLetterInAnswer:sender.titleLabel.text];
    [self animateCheking:sender stateOfFinding:result];
   
  
}
-(void) animateCheking: (UIButton*) sender stateOfFinding: (BOOL) state
{
    if(countOfAttempts<6)
    {
    CAShapeLayer *crossLayer = [CAShapeLayer layer];
    [arrayOfCrossMarks addObject:crossLayer];
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, sender.bounds.size.width, 0);
    CGPathAddLineToPoint(path, NULL, 0, sender.bounds.size.height);
    CGPathMoveToPoint(path, NULL, 0, 0);
    CGPathAddLineToPoint(path, NULL, sender.bounds.size.width, sender.bounds.size.height);
    crossLayer.path = path;
    if (state) {
         crossLayer.strokeColor = [UIColor greenColor].CGColor;
    }
    else{
         crossLayer.strokeColor = [UIColor redColor].CGColor;
    }
    crossLayer.backgroundColor = [[UIColor clearColor] CGColor];
    crossLayer.fillColor = [UIColor clearColor].CGColor;
    crossLayer.opaque = NO;
    sender.opaque = NO;
    //
    [sender.layer addSublayer:crossLayer];
    CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    pathAnimation.duration = 1.0;
    pathAnimation.removedOnCompletion = NO;
    pathAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
    pathAnimation.toValue = [NSNumber numberWithFloat:1.0f];
    [crossLayer addAnimation:pathAnimation forKey:@"strokeEndAnimation"];
     sender.enabled = NO;
    CGPathRelease(path);
    }
    else{
         countOfAttempts = 0;
        
    }
    
}
-(void) resetButtonsAndMarks
{
    for (UIButton* button in arrayOfButtons) {
        button.enabled = YES;
    }
    for (CAShapeLayer* layer in arrayOfCrossMarks) {
        [layer removeFromSuperlayer];
        [layer removeAnimationForKey:@"strokeEndAnimation"];
        
    }
     [hangmanDrawingView resetSublayers]; //clear the drawing view
    [self getRandomQuestion];
    [self recoverHintsAfterChangeQuestion];
    
}
-(BOOL) checkLetterInAnswer: (NSString*) letter
{
    BOOL isAnswerFound = NO;
    for (NSUInteger i=0;i<[currentQuestion.answer length];i++)
    {
        NSRange symbolRange = NSMakeRange(i, 1);
        if ([[currentQuestion.answer substringWithRange:symbolRange] isEqualToString:[letter lowercaseString]])
        {
            NSLog(@"found: %lu", (unsigned long)i);
            isAnswerFound = YES;
            //
            NSIndexPath *path = [NSIndexPath indexPathForItem:i inSection:0];
            AlphabeticCollectionViewCell* cell = (AlphabeticCollectionViewCell*)[_answerLetterCollection cellForItemAtIndexPath:path];
            if (!cell.openState) {
                [cell changeState];
            }
            
        }
      
    }
    if (!isAnswerFound) {
        countOfAttempts++;
        [hangmanDrawingView animateStrokeEnd:countOfAttempts];
        if (countOfAttempts>=6) {
            //if player lose
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"You lose!"
                                                                           message:@"Please, try again..."
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {
                                                                 [self resetButtonsAndMarks];
                                                                 [self getRandomQuestion];
                                                             }];
            
            
            [alert addAction:okAction];
       
            [self presentViewController:alert animated:YES completion:nil];
          
            NSLog(@"You lose! Please, try again");
        }
    }
    [self performSelector:@selector(checkAnswerFilled) withObject:nil afterDelay:1.0];
    return isAnswerFound;

}
-(void) checkAnswerFilled
{
    NSArray* arrayOfCells = [_answerLetterCollection visibleCells];
    NSInteger count = 0;
    for (AlphabeticCollectionViewCell* cell in arrayOfCells) {
        if (cell.openState) {count++;}
    }
    if (count == arrayOfCells.count) {
        [self getRandomQuestion];
        [self resetButtonsAndMarks];
        countOfAttempts = 0;
        
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) getRandomQuestion
{
    NSInteger questionsCount = [questionsInCategory count];
    if (questionsCount!=0)
    {
    NSInteger randomIndex = arc4random_uniform((u_int32_t )questionsCount);
    currentQuestion = [questionsInCategory objectAtIndex:randomIndex];
    _questionTextLabel.text = currentQuestion.question;
    _questionTextLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _questionTextLabel.numberOfLines = 0;
    
    [_answerLetterCollection reloadData];
    }
   
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
   return currentQuestion.answer.length;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
 
    return CGSizeMake(25, 50);
}
- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    AlphabeticCollectionViewCell* cell = (AlphabeticCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"AlphabethicalCell" forIndexPath:indexPath];
    NSRange symbolRange = NSMakeRange(indexPath.item, 1);
    cell.textLabel.text = [[currentQuestion.answer substringWithRange:symbolRange] uppercaseString];
    [cell recoverCell];
    if([cell.textLabel.text isEqualToString:@" "]){ [cell changeState]; cell.hidden= YES;}
    
    return cell;
}
@end
