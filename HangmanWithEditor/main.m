//
//  main.m
//  HangmanWithEditor
//
//  Created by User on 11/23/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
