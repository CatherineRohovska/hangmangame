//
//  AlphabeticCollectionViewCell.h
//  HangmanWithEditor
//
//  Created by User on 11/24/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlphabeticCollectionViewCell : UICollectionViewCell
@property(nonatomic, strong) UILabel* textLabel;
@property(nonatomic) bool openState;

- (void) changeState;
-(void) recoverCell;
@end
