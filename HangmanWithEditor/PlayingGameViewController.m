//
//  PlayingGameViewController.m
//  HangmanWithEditor
//
//  Created by User on 11/23/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "PlayingGameViewController.h"
#import "DataManager.h"
#import "GameViewController.h"
@interface PlayingGameViewController ()
{
    NSMutableArray* categories;
}
@end

@implementation PlayingGameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    DataManager* manager =  [DataManager sharedManager];
    
    categories = [manager getCategories];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return categories.count;//gameManager.maxLevelReached;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"CategorySelectionCell" forIndexPath:indexPath];
    cell.backgroundColor = [[UIColor blueColor] colorWithAlphaComponent:0.25];
    CategoryEntry* ctg  = [categories objectAtIndex:indexPath.item];
    DataManager* manager =  [DataManager sharedManager];
    NSMutableArray* array = [manager getQuestionsInCategory:ctg.name];
    NSInteger countQuestions = array.count;
    cell.textLabel.text = [[[ctg.name  stringByAppendingString:@" ("] stringByAppendingString:[@(countQuestions) stringValue]] stringByAppendingString:@")"];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    CategoryEntry* ctg  = [categories objectAtIndex:indexPath.item];
    GameViewController* ctrl = [self.storyboard instantiateViewControllerWithIdentifier:@"GameViewController"];
    ctrl.category = ctg.name;
    DataManager* manager = [DataManager sharedManager];
    if([[manager getQuestionsInCategory:ctg.name] count ]!=0)
    {
    [self.navigationController pushViewController:ctrl animated:YES];
    }

    
}

-(void)dealloc
{
    NSLog(@"PlayingGameController dealloced");
}
@end
