//
//  ViewController.m
//  HangmanWithEditor
//
//  Created by User on 11/23/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "ViewController.h"
#import "DataManager.h"
#import "AddingQuestionsViewController.h"
#import "PlayingGameViewController.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [DataManager sharedManager]; //first call and initialization
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (IBAction)addingQuestions:(id)sender {
    AddingQuestionsViewController* ctrl = [self.storyboard instantiateViewControllerWithIdentifier:@"adding_question"];
    [self.navigationController pushViewController:ctrl animated:YES];
}

- (IBAction)playingGame:(id)sender {
    PlayingGameViewController* ctrl = [self.storyboard instantiateViewControllerWithIdentifier:@"playing_game"];
    [self.navigationController pushViewController:ctrl animated:YES];
}
@end
