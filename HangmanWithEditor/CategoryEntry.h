//
//  CategoryEntry.h
//  HangmanWithEditor
//
//  Created by User on 11/23/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface CategoryEntry : NSManagedObject
@property (nonatomic, strong) NSString* name;
@end
