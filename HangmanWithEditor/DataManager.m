//
//  DataManager.m
//  HangmanWithEditor
//
//  Created by User on 11/23/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "DataManager.h"

@implementation DataManager
{
    NSEntityDescription* questionEntity;
    NSEntityDescription* categoryEntity;
}
+ (id)sharedManager {
    static DataManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *ncontext = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        ncontext = [delegate managedObjectContext];
    }
    return ncontext;
}
- (id)init {
    if (self = [super init]) {
        
        //initiate
        _context = [self managedObjectContext];
        questionEntity = [NSEntityDescription entityForName:@"Question" inManagedObjectContext:_context];
        categoryEntity = [NSEntityDescription entityForName:@"Category" inManagedObjectContext:_context];
        //first loading app
        static NSString* const hasRunAppOnceKey = @"HangmanGameFirstLaunch";
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        
        if (![defaults boolForKey:hasRunAppOnceKey])
        {
           
            [defaults setBool:YES forKey:hasRunAppOnceKey];
            [self createBaseQuestions];
        }
       
    }
    return self;
}
-(void) createBaseQuestions
{
    NSError *error = nil;
    NSString* jsonData = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"initiateQuestions" ofType:nil] encoding:NSUTF8StringEncoding error:&error];
    //creating data
    NSDictionary * allData = [NSJSONSerialization JSONObjectWithData:[jsonData dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&error];
    NSArray* allQuestions = [allData objectForKey:@"Questions"];
    for (int i=0; i<allQuestions.count; i++)
    {
        NSDictionary* currentQuestion = allQuestions[i];
        NSString* question = [currentQuestion objectForKey:@"Question"];
        NSString* answer = [currentQuestion objectForKey:@"Answer"];
        NSString* category = [currentQuestion objectForKey:@"Category"];
        [self createNewCategory:category];
        [self createQuestion:question withAnswer:answer andCategory:category];
    }
}

-(void) createQuestion: (NSString*) question withAnswer: (NSString*) answer andCategory: (NSString*) category
{
    NSManagedObject *newQuestion = [[NSManagedObject alloc] initWithEntity:questionEntity insertIntoManagedObjectContext:_context];
     NSString* questionModified = [question stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    [newQuestion setValue:questionModified forKey:@"question"];
   
    [newQuestion setValue:[[answer lowercaseString] stringByTrimmingCharactersInSet:
     [NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"answer"] ;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    [fetchRequest setEntity:categoryEntity];
    
    NSError *error;
    NSArray *items = [_context executeFetchRequest:fetchRequest error:&error];
    
    NSManagedObject* ctg = nil;
    for (CategoryEntry *managedObject in items) {
        if ([managedObject.name isEqualToString:category]) {
            ctg = managedObject;
            break;
        }
    }
    if (!ctg) {
        ctg = [[NSManagedObject alloc] initWithEntity:categoryEntity insertIntoManagedObjectContext:_context];
        [ctg setValue:[category lowercaseString] forKey:@"name"];
        NSError *error = nil;
        if (![ctg.managedObjectContext save:&error]) {
            NSLog(@"Unable to save Category.");
            NSLog(@"%@, %@", error, error.localizedDescription);
        }
    }
    else
    {
        NSMutableSet *questions = [ctg mutableSetValueForKey:@"questions"];
        [questions addObject:newQuestion];
        
    }
 
    //saving context
    if (![newQuestion.managedObjectContext save:&error]) {
        NSLog(@"Unable to save Question");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
}
-(void) createNewCategory:(NSString*) name
{
    
    NSString* newCategoryName = [[name lowercaseString] stringByTrimmingCharactersInSet:
            [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (![self isCategoryExists:newCategoryName])
    {
     NSManagedObject *newCategory = [[NSManagedObject alloc] initWithEntity:categoryEntity insertIntoManagedObjectContext:_context];
    [newCategory setValue:newCategoryName  forKey:@"name"];
    NSError *error = nil;
    if (![newCategory.managedObjectContext save:&error]) {
        NSLog(@"Unable to save Category.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    }
    
}
-(BOOL) isCategoryExists: (NSString*) categoryName
{
    bool res = NO;
    NSMutableArray* array = [self getCategories];
    for (CategoryEntry* ctg in array) {
        if ([ctg.name isEqualToString:categoryName])
        {
            res = YES;
        }
    }
    return res;
}
-(NSMutableArray*) getCategories{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Category"];
    
    return [[_context executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
}
-(NSMutableArray*) getQuestions{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Question"];
    
    return [[_context executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
}
-(void) deleteAllCategories
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];

    [fetchRequest setEntity:categoryEntity];
    
    NSError *error;
    NSArray *items = [_context executeFetchRequest:fetchRequest error:&error];
    
    
    for (NSManagedObject *managedObject in items) {
        [_context deleteObject:managedObject];
       
    }
    if (![_context save:&error]) {
        NSLog(@"Unable to save deleted Category.");
        NSLog(@"%@, %@", error, error.localizedDescription);

    }
}
-(void) deleteAllQuestions
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    [fetchRequest setEntity:questionEntity];
    
    NSError *error;
    NSArray *items = [_context executeFetchRequest:fetchRequest error:&error];
    
    
    for (NSManagedObject *managedObject in items) {
        [_context deleteObject:managedObject];
        
    }
    if (![_context save:&error]) {
        NSLog(@"Unable to save deleted Questions.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    }
}
-(NSMutableArray*) getQuestionsInCategory: (NSString*) name
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Question"];
    // Create Predicate
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"categorized.name", name];
    [fetchRequest setPredicate:predicate];
    

    return [[_context executeFetchRequest:fetchRequest error:nil] mutableCopy];
}
@end
