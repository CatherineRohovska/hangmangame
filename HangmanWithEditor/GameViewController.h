//
//  GameViewController.h
//  HangmanWithEditor
//
//  Created by User on 11/24/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameViewController : UIViewController
<UIScrollViewDelegate>
@property (nonatomic, strong) NSString* category;
@property (weak, nonatomic) IBOutlet UILabel *questionTextLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *answerLetterCollection;
//@property (weak, nonatomic) IBOutlet UIProgressView *restOfTryesProgressView;
//@property (weak, nonatomic) IBOutlet UILabel *tryesMarkLabel;
@property (weak, nonatomic) IBOutlet UIView *hangmanPlaceholderView;

@end
