//
//  AddingQuesrtionsViewController.h
//  HangmanWithEditor
//
//  Created by User on 11/23/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddingQuestionsViewController : UIViewController
<UITableViewDelegate, UITextFieldDelegate, UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableOfCategories;
@property (weak, nonatomic) IBOutlet UITextView *questionText;
@property (weak, nonatomic) IBOutlet UITextField *answerText;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraintToAnswerText;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraintToAddingButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraintToQuestionText;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraintToCategoryTables;
- (IBAction)addQuestionClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *addQuestionButton;
- (IBAction)doneEditingTextView:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraintEditingTextView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTextViewConstraint;

@end
