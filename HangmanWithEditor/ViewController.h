//
//  ViewController.h
//  HangmanWithEditor
//
//  Created by User on 11/23/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (IBAction)addingQuestions:(id)sender;
- (IBAction)playingGame:(id)sender;

@end

