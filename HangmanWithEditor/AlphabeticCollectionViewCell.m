//
//  AlphabeticCollectionViewCell.m
//  HangmanWithEditor

//  Created by User on 11/24/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "AlphabeticCollectionViewCell.h"

@implementation AlphabeticCollectionViewCell
{
     UIView* coverView;
}
-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self){
        
        _textLabel = [[UILabel alloc] initWithFrame:self.bounds];
        _textLabel.textAlignment = NSTextAlignmentCenter;
        _textLabel.textColor = [UIColor whiteColor];
        _textLabel.backgroundColor = [UIColor blueColor];
        _textLabel.autoresizingMask = (UIViewAutoresizingFlexibleWidth| UIViewAutoresizingFlexibleHeight);
        [self.contentView addSubview:_textLabel];
        
        coverView = [[UIView alloc] initWithFrame:self.bounds];
        coverView.autoresizingMask = (UIViewAutoresizingFlexibleWidth| UIViewAutoresizingFlexibleHeight);
        coverView.backgroundColor = [UIColor magentaColor];
        
        self.backgroundColor = self.superview.backgroundColor;
        self.hidden = NO;
        _openState = NO;
    }
    return self;
}
- (void) changeState
{
    self.userInteractionEnabled = NO;
    [UIView transitionFromView:coverView toView: _textLabel duration:1.0
                       options:UIViewAnimationOptionTransitionFlipFromRight
                    completion:^(BOOL finished) {
                        self.userInteractionEnabled = YES;
                    }];
    _openState = YES;
}
-(void) recoverCell
{
     [self.contentView addSubview:coverView];
     _openState = NO;
    self.hidden = NO;
}
@end
