//
//  AddingQuesrtionsViewController.m
//  HangmanWithEditor
//
//  Created by User on 11/23/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "AddingQuestionsViewController.h"
#import "DataManager.h"
@interface AddingQuestionsViewController ()
{
    NSMutableArray* categories;
    NSString* categoryName;
    DataManager* manager;
    NSString* newCategoryName;
}
@end

@implementation AddingQuestionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    manager =  [DataManager sharedManager];
    
    categories = [manager getCategories];
    
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"Add category" style:UIBarButtonItemStylePlain target:self action:@selector(refreshCategoriesList)];
    self.navigationItem.rightBarButtonItem = anotherButton;
    
    _answerText.delegate = self;
    _questionText.delegate = self;
   
    NSInteger delta = self.view.bounds.size.height - 100 -(_questionText.bounds.size.height+_answerText.bounds.size.height+50);
     NSLog(@"%f", _heightTextViewConstraint.constant);
    _heightTextViewConstraint.constant = _heightTextViewConstraint.constant+delta;
     NSLog(@"%ld",(long) _heightTextViewConstraint.constant);
    //  NSLog(@"%ld",(long) _topConstraintToAddingButton.constant);
    _topConstraintToAddingButton.constant = _topConstraintToAddingButton.constant+delta;
  //  NSLog(@"%ld",(long) _topConstraintToAddingButton.constant);
 
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //take notifications on event describes keyboard apperense and dissapearense
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    //remove notifications on event describes keyboard apperense and dissapearense
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                             name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
}
- (void)keyboardWillShow:(NSNotification*)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    if (_answerText.isFirstResponder /*|| _questionText.isFirstResponder*/){
        
        _topConstraintToCategoryTables.constant = _topConstraintToCategoryTables.constant - keyboardSize.height;
        _topConstraintToQuestionText.constant = _topConstraintToQuestionText.constant-keyboardSize.height;
        _topConstraintEditingTextView.constant = _topConstraintEditingTextView.constant - keyboardSize.height;
       
        _addQuestionButton.enabled = NO;
    }
    if (_questionText.isFirstResponder) {
       
        NSInteger delta =fabs( _questionText.frame.origin.y-(self.view.bounds.size.height - keyboardSize.height));
         NSLog(@" delta: %ld", (long)delta);
        _heightTextViewConstraint.constant = _heightTextViewConstraint.constant-delta;
        NSLog(@"%ld",(long) _heightTextViewConstraint.constant);
    }
}

- (void)keyboardWillHide:(NSNotification*)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    if (_answerText.isFirstResponder /*|| _questionText.isFirstResponder*/){
        
        _topConstraintToCategoryTables.constant = _topConstraintToCategoryTables.constant + keyboardSize.height;
        _topConstraintToQuestionText.constant = _topConstraintToQuestionText.constant+keyboardSize.height;
       _topConstraintEditingTextView.constant = _topConstraintEditingTextView.constant + keyboardSize.height;
        _bottomConstraintToAnswerText.constant = _bottomConstraintToAnswerText.constant-keyboardSize.height;
        
        _addQuestionButton.enabled = YES;
    }
    if (_questionText.isFirstResponder) {
        NSInteger delta = (self.view.bounds.size.height - keyboardSize.height)/2;
        _heightTextViewConstraint.constant = _heightTextViewConstraint.constant+delta;
        NSLog(@"%ld",(long) _heightTextViewConstraint.constant);
    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
//tableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return categories.count;//gameManager.maxLevelReached;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"CategoryCell" forIndexPath:indexPath];
    cell.backgroundColor = [[UIColor blueColor] colorWithAlphaComponent:0.25];
    CategoryEntry* ctg  = [categories objectAtIndex:indexPath.item];
    cell.textLabel.text = ctg.name;
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   // UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    CategoryEntry* ctg  = [categories objectAtIndex:indexPath.item];
    categoryName = ctg.name;//cell.textLabel.text;
  
}
-(void) refreshCategoriesList
{

    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Creation of a new category"
                                                                   message:@"Please, enter category name"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                         UITextField* txt = [alert.textFields objectAtIndex:0];
                                                         if ([txt.text isEqualToString:@""]) {
                                                             //if field is empty
                                                            
                                                         }
                                                         else{
                                                             newCategoryName =txt.text;
                                                             [manager createNewCategory:newCategoryName];
                                                             categories = [manager getCategories];
                                                             [_tableOfCategories reloadData];
                                                            // newCategoryName = nil;

                                                         }
                                                                                                              }];
    
    
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            textField.delegate = self;
            textField.placeholder = @"Enter category name";
        textField.keyboardType = UIKeyboardTypeAlphabet;
    }];
    [alert addAction:okAction];
 // [alert.view setNeedsUpdateConstraints];
    [alert.view setNeedsLayout];
    [self presentViewController:alert animated:YES completion:^{
       // [alert.textFields[0] becomeFirstResponder];
    }];
    
}
-(void)dealloc
{
    NSLog(@"AddingQuestionController dealloced");
}
- (IBAction)addQuestionClick:(id)sender {
    if (![_questionText.text isEqualToString:@""]&&![_answerText.text isEqualToString:@""]&&![categoryName isEqualToString:@""]) {
        [manager createQuestion:_questionText.text withAnswer:_answerText.text andCategory:categoryName];
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Adding new question"
                                                                       message:@"Question has been added successfully"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                                 NSMutableArray* array = [manager getQuestions];
                                                                 for ( QuestionEntry* ctg in array) {
                                                                     NSLog(@"Question: %@", ctg.question);
                                                                 }
                                                         }];
        
        
        [alert addAction:okAction];
    
        [self presentViewController:alert animated:YES completion:nil];
    }
    else{
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Some fields are empty"
                                                                       message:@"Please, fill the fields or select a category"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             
                                                         }];
        
        
        [alert addAction:okAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
  
}
//textfield delegate for answer and category name
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
 
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 15) ? NO : YES;
 
  
}
- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    
    if (theTextField == _answerText) {
        
        [theTextField resignFirstResponder];
        
    }
//    if (theTextField ==(UITextField *) _questionText) {
//         [theTextField resignFirstResponder];
//    }
//    
    return YES;
    
}

- (IBAction)doneEditingTextView:(id)sender {
    [_questionText resignFirstResponder];
}
-(void)textViewDidChange:(UITextView *)textView
{
//textView.
   // [textView setContentOffset:CGPointMake(0, 0)];
}
@end
