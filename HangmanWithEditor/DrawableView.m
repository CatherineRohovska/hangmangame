//
//  DrawableView.m
//  HangmanWithEditor
//
//  Created by User on 11/25/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "DrawableView.h"

@implementation DrawableView
{
    CGFloat maxY;
    CGFloat basePointX;
    CGFloat stickPointX;
    CGFloat handsPointX;
    CGFloat handsPointY;
    CGFloat bodyPointX;
    CGFloat lineHorizontal;
    CGFloat lineVertical;
    CGFloat headX;
    CGFloat headY;
    CGFloat radiusHead;
    UIImageView* pencil;
    NSMutableArray* layersArray;
    CGMutablePathRef path;
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.*/
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        CGFloat delta = 2;
        maxY = self.bounds.size.height;
        lineHorizontal = (self.bounds.size.width-delta)/3;
        lineVertical = maxY/4;
        basePointX = 0+lineHorizontal;
        stickPointX = lineHorizontal*1.5;
        bodyPointX = lineHorizontal*2.5+delta;
       
        handsPointX = bodyPointX-lineVertical/2;
        handsPointY = maxY - lineVertical*2.5;
        if(lineVertical<lineHorizontal)
        {
       
        headY = maxY-3*lineVertical-lineVertical/2;
            radiusHead = lineVertical;
            headX = bodyPointX;//- radiusHead/2;
        }
        else
        {
            
            headY = maxY-3*lineVertical-lineVertical/2;
            radiusHead = lineHorizontal;
            headX = bodyPointX;//- radiusHead/2;
            
            
        }
      pencil = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, lineVertical/3, lineVertical/3)];
        layersArray = [[NSMutableArray alloc] init];
          [self addSubview:pencil];
        pencil.hidden = YES;
        pencil.layer.anchorPoint = CGPointMake(0, 1);
        pencil.image = [UIImage imageNamed:@"pencil"];
        pencil.backgroundColor = [UIColor clearColor];
    }
    return self;
}


-(void)animateStrokeEnd: (NSInteger) stage {
    if (stage!=0)
    {
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
       
    path = CGPathCreateMutable();
    //creating a path
        //base
        if (stage==1) {
    CGPathMoveToPoint(path, NULL, basePointX, maxY);
    CGPathAddLineToPoint(path,NULL, basePointX, maxY-lineVertical);
    CGPathAddLineToPoint(path,NULL, basePointX+lineHorizontal, maxY-lineVertical);
    CGPathAddLineToPoint(path,NULL, basePointX+lineHorizontal, maxY);
        }
    // stick
        if (stage==2) {
    CGPathMoveToPoint(path, NULL, stickPointX, maxY-lineVertical);
    CGPathAddLineToPoint(path, NULL, stickPointX, maxY-maxY);
    CGPathAddLineToPoint(path, NULL, stickPointX+lineHorizontal+5, maxY-maxY);
        }
    //head
        if (stage==3) {
    CGPathMoveToPoint(path, NULL, headX+radiusHead/2, headY);
    CGPathAddArc(path, NULL, headX+radiusHead/2-radiusHead/2, headY,radiusHead/2,0, 2*M_PI, NO);
        }
    // body
        if (stage==4) {
    CGPathMoveToPoint(path, NULL, bodyPointX, maxY-3*lineVertical);
    CGPathAddLineToPoint(path, NULL, bodyPointX, maxY-2*lineVertical);
        }
   //legs
        if (stage==5) {
    CGPathMoveToPoint(path, NULL, bodyPointX, maxY-2*lineVertical);
    CGPathAddLineToPoint(path, NULL, bodyPointX-lineHorizontal/3, maxY-lineVertical);
    CGPathMoveToPoint(path, NULL, bodyPointX, maxY-2*lineVertical);
    CGPathAddLineToPoint(path,  NULL, bodyPointX+lineHorizontal/3, maxY-lineVertical);
        }

    //hands
        if (stage==6) {
    CGPathMoveToPoint(path,  NULL, handsPointX, handsPointY);
    CGPathAddLineToPoint(path,  NULL, handsPointX+lineVertical, handsPointY);
    
        }
    shapeLayer.path = path;
    shapeLayer.strokeColor = [UIColor blueColor].CGColor;
    shapeLayer.backgroundColor = [[UIColor clearColor] CGColor];
    self.backgroundColor = [UIColor clearColor];
    shapeLayer.fillColor = [UIColor clearColor].CGColor;
    shapeLayer.opaque = NO;
    self.opaque = NO;
    //
    [self.layer addSublayer:shapeLayer];
    CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    pathAnimation.duration = 2.0;
    pathAnimation.removedOnCompletion = NO;
    pathAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
    pathAnimation.toValue = [NSNumber numberWithFloat:1.0f];
        pathAnimation.delegate = self;
    [shapeLayer addAnimation:pathAnimation forKey:@"strokeEndAnimation"];
    
    
    
    CAKeyframeAnimation * theAnimation;

    theAnimation=[CAKeyframeAnimation animationWithKeyPath:@"position"];
    
    theAnimation.path=path;
    theAnimation.duration=2.0;
    theAnimation.removedOnCompletion = NO;
    theAnimation.calculationMode = kCAAnimationPaced;
        theAnimation.fillMode = kCAFillModeForwards;
        theAnimation.cumulative = YES;
    theAnimation.delegate = self;
    
    pencil.hidden = NO;
  
    [pencil.layer addAnimation:theAnimation forKey:@"position"];
    [layersArray addObject:shapeLayer];
      
      
     CGPathRelease(path);
    }
}


- (void) animationCompleteImmediately
{

    if(layersArray.count>0){
        for (NSInteger i =0; i<layersArray.count; i++) {
            CAShapeLayer* layer = [layersArray objectAtIndex:i];
            [layer removeAnimationForKey:@"strokeEndAnimation"];
          
        }
        [pencil.layer removeAnimationForKey:@"position"];
        pencil.hidden = YES;
  
    }
}
-(void) resetSublayers
{
    for (CAShapeLayer* layer in layersArray) {
        [layer removeFromSuperlayer];
     }
    layersArray = nil;
    layersArray = [[NSMutableArray alloc] init];
    pencil.hidden = YES;
    
}
@end
